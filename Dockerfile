ARG UBUNTU_VERSION=20.04

FROM ubuntu:$UBUNTU_VERSION AS builder

ENV FFMPEG_VERSION 4.3

# FFMPEG dependencies
ENV BUILD_DEPS_FFMPEG "autoconf automake build-essential cmake git-core libopus-dev libogg-dev librtmp-dev libsdl2-dev libtool libc6 libc6-dev unzip libv4l-dev libvdpau-dev libvpx-dev libwebp-dev libx264-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev nasm yasm pkg-config texinfo wget zlib1g-dev libnuma1 libnuma-dev libvorbis-dev libfdk-aac-dev"

# Update packages and install package creator
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y checkinstall tzdata && mkdir /tmp/packages

# Installing ffmpeg
RUN set -x \
  && apt-get install -y --no-install-recommends --no-install-suggests ${BUILD_DEPS_FFMPEG} \
  && cd /tmp/ \
  && wget http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz \
  && tar zxf ffmpeg-${FFMPEG_VERSION}.tar.gz \
  && cd /tmp/ffmpeg-${FFMPEG_VERSION} \
  && ./configure \
    --disable-debug \
    --disable-static \
    --disable-stripping \
    --enable-avfilter \
    --enable-avresample \
    --enable-decoder=libvorbis \
    --enable-demuxer=ogg \
    --enable-encoder=libvorbis \
    --enable-gpl \
    --enable-libfdk-aac \
    --enable-libx264 \
    --enable-libopus \
    --enable-librtmp \
    --enable-libv4l2 \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libwebp \
    --enable-muxer=ogg \
    --enable-nonfree \
    --enable-pic \
    --enable-postproc \
    --enable-pthreads \
    --enable-shared \
    --enable-small \
    --enable-version3 \
  && make \
  && checkinstall \
      --default \
      --install=no \
      --nodoc \
      --pakdir=/tmp/packages \
      --pkgname=ffmpeg \
      --pkgversion=$FFMPEG_VERSION \
      --type=debian \
        make install

FROM ubuntu:$UBUNTU_VERSION

RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
    libxv1 \
    x264 \
    libsdl2-2.0-0 \
    libv4l-0 \
    libxcb-shm0 \
    libxcb-shape0 \
    libxcb-xfixes0 \
    libvdpau1 \
    libvpx6 \
    libogg0 \
    opus-tools \
    libfdk-aac1 \
    openssh-server \
    librtmp1 \
    libsndio7.0

COPY --from=builder /tmp/packages /tmp/packages

RUN dpkg -i /tmp/packages/*.deb

# base image overwrite this env and ffmpeg install shared libraries to /usr/local/lib, so we need to add
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/local/lib"
